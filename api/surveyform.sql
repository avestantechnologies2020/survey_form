-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2021 at 06:38 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `badabazarr`
--

-- --------------------------------------------------------

--
-- Table structure for table `surveyform`
--

CREATE TABLE `surveyform` (
  `id` int(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(250) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `City` varchar(250) NOT NULL,
  `Pincode` int(10) NOT NULL,
  `Area` varchar(255) NOT NULL,
  `MonthExpenses` int(255) NOT NULL,
  `Accupation` varchar(255) NOT NULL,
  `MobileNumber` bigint(255) NOT NULL,
  `FavoriteStore` varchar(255) NOT NULL,
  `CustomerShopping` varchar(255) NOT NULL,
  `Download` varchar(255) NOT NULL,
  `BuyIntrest` varchar(10000) NOT NULL,
  `State` varchar(255) NOT NULL,
  `RfNumber` int(255) NOT NULL,
  `Dates` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surveyform`
--

INSERT INTO `surveyform` (`id`, `FirstName`, `LastName`, `Address`, `City`, `Pincode`, `Area`, `MonthExpenses`, `Accupation`, `MobileNumber`, `FavoriteStore`, `CustomerShopping`, `Download`, `BuyIntrest`, `State`, `RfNumber`, `Dates`) VALUES
(50, 'Ritesh', 'shedmake', 'visapur', 'chandrapur', 442401, 'chandrapur', 15000, 'developer', 5584265425, 'haldiram', 'Both', ' no', 'Daily Needs,Cloths Shop / Fashion Wears,Foot Wears', 'Maharashtra', 123, '2021-01-09'),
(51, 'rohit', 'wankar', 'green city', 'nagpur', 440022, 'jamtha', 50000, 'senior developer', 5635265652, 'bigbazaar', 'Both', ' no', 'Books,Mobile Stores,Foot Wears', 'Maharashtra', 456, '2021-01-09'),
(52, 'sudhir', 'shahare', 'nagpur', 'nagpur', 440022, 'nagpur', 562563, 'developer', 5991656616, 'bigbazaar', 'Online', ' no', 'Bakery,Daily Needs,Cloths Shop / Fashion Wears', 'Maharashtra', 865, '2021-01-09'),
(53, 'rohan', 'wankar', 'rajura', 'nagpur', 440022, 'nahpur', 464646, 'developer', 6566464646, 'bigbazaar', 'Both', 'yes', 'H/W Shops,Foot Wears,Mobile Stores,Event Management', 'Maharashtra', 64664, '2021-01-09'),
(54, 'ajinkya', 'kohle', 'nagpur', 'nagpur', 440022, 'djdsjbvjs', 91561515, 'developer', 4165651616, 'dvsnsvjsvnjsvsvjs', 'Both', 'yes', 'Bakery,Daily Needs,Cloths Shop / Fashion Wears,Mobile Stores,Foot Wears,Hotels & Dhabas / Restaurant', 'Maharashtra', 565, '2021-01-09'),
(55, 'sumit', 'shahare', 'gondia', 'gondia', 441601, 'vbjsbsj', 646316, 'dkvdkvdk', 546565465, 'dsknsdv', 'Offline', 'yes', 'Electronics / Home Appliances,Bakery,Mobile Stores', 'Maharashtra', 4545154, '2021-01-11'),
(56, 'vaibhav', 'nimje', 'bhandara', 'bhandara', 441904, 'dvjdjsjs', 246541, 'sjvjsdjs', 144514684, 'vdjdjsdsb', 'Offline', 'yes', 'Electronics / Home Appliances,Bakery,Daily Needs,Electronics / Home Appliances,Bakery,Daily Needs,Cloths Shop / Fashion Wears,Books,Mobile Stores,Foot Wears,H/W Shops,Event Management,Hotels & Dhabas / Restaurant,Pharmacy,Agriculture Fertilizer / Machiner', 'dsjsnsj', 15455, '2021-01-11'),
(61, 'avinash', 'todase', 'dvdjsbvjsdvb', 'wardha', 442001, 'vnsjvsj', 416415, 'dvsjbvsj', 52641844, 'dknjsfjdf', 'Offline', 'yes', 'Electronics / Home Appliances,Bakery,Daily Needs,Cloths Shop / Fashion Wears,Books,Mobile Stores,Foot Wears,H/W Shops,Event Management,Hotels & Dhabas / Restaurant,Pharmacy,Agriculture Fertilizer / Machinery,AutoMobile Sale / Car-Bike,AutoMobile Services,', 'Maharashtra', 424, '2021-01-11'),
(62, 'gaurav', 'varma', 'amravati', 'amravati', 444601, 'amravati', 512512, 'developer', 564664946, 'scssbsjbcjsb', 'Both', 'yes', 'Electronics / Home Appliances,Bakery,Daily Needs,Cloths Shop / Fashion Wears,Books,Mobile Stores,Foot Wears,H/W Shops,Event Management,Hotels & Dhabas / Restaurant,Pharmacy,Agriculture Fertilizer / Machinery,AutoMobile Sale / Car-Bike,AutoMobile Services,', 'Maharashtra', 56656, '2021-01-11'),
(63, 'rakesh', 'tonge', 'nagpur', 'nagpur', 440022, 'nagpur', 54555, 'shghs', 941644, 'bjdagdua', 'Both', 'yes', 'Electronics / Home Appliances,Bakery,Daily Needs,Cloths Shop / Fashion Wears,Books,Mobile Stores,Foot Wears,H/W Shops,Event Management,Hotels & Dhabas / Restaurant,Pharmacy,Agriculture Fertilizer / Machinery,AutoMobile Sale / Car-Bike,AutoMobile Services,Battery & Inverter,Furniture,Gold & Silver,Surgical Instruments,Lights & Jhoomers,Hardware,Herbal Care Products,Home / Building Materials,Musical Instruments,Glass & Aluminiums,Others', 'dddd', 545454, '2021-01-12'),
(64, 'roshan', 'pawade', 'chandrapur', 'chandrapur', 440022, 'ballarpur', 666166, 'ftfffft', 6546664, 'ctttj', 'Both', 'yes', 'Electronics / Home Appliances,Bakery,Daily Needs,Cloths Shop / Fashion Wears,Books,Mobile Stores,Foot Wears,H/W Shops,Event Management,Hotels & Dhabas / Restaurant,Pharmacy,Agriculture Fertilizer / Machinery,AutoMobile Sale / Car-Bike,AutoMobile Services,Battery & Inverter,Furniture,Gold & Silver,Surgical Instruments,Lights & Jhoomers,Hardware,Herbal Care Products,Home / Building Materials,Musical Instruments,Glass & Aluminiums,Others', 'hvhy', 15515, '2021-01-12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `surveyform`
--
ALTER TABLE `surveyform`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `surveyform`
--
ALTER TABLE `surveyform`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
