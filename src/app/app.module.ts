import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms'
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ViewDataComponent } from './view-data/view-data.component';
import { RouterModule, Routes } from '@angular/router';
import { SurveyFormComponent } from './survey-form/survey-form.component';
import { LoginComponent } from './login/login.component';

import { SurveyService } from './survey.service';

const routes: Routes = [
  {path:"",component:SurveyFormComponent},
  {path:"admin",component:ViewDataComponent,canActivate:[SurveyService]},
  {path:"login",component:LoginComponent},
  
];

@NgModule({
  declarations: [
    AppComponent,
    ViewDataComponent,
    SurveyFormComponent,
    LoginComponent,
    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgMultiSelectDropDownModule,
    RouterModule.forRoot(routes)
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
