import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SurveyService implements CanActivate{

  constructor(private _http:HttpClient) { }
  
  insertdata(ob){
   return this._http.post("http://localhost/api/insert.php",ob);
  }
 
  fetchdata(){
    return this._http.get("http://localhost/api/fetchapi.php");
   }
   canActivate(){
    if(localStorage.getItem("aut"))
    return true;
    else
    return false;
  }

}
