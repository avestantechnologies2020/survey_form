import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SurveyService } from '../survey.service';

@Component({
  selector: 'app-view-data',
  templateUrl: './view-data.component.html',
  styleUrls: ['./view-data.component.scss']
})
export class ViewDataComponent implements OnInit {

  data;
  tmp:string="";
  tmp1:number=0;
  dummy={"data":[]}
  Pincode
  cat:string=""

  constructor(private _survey:SurveyService,private _router:Router){
    this.get()
    }

  city(event){
    this.tmp = event.target.value
    this.tmp1=1
    this.dummy.data=[];
    this.Pincode="";
    for(var i=0; i<this.data.length;i++){
        if(this.tmp ==this.data[i].City){
          this.dummy.data.push(this.data[i])
        }else if(this.tmp==""){
          this.dummy.data.push(this.data[i])
        }
    }
  }

  pin(){
    this.tmp1=1
    this.dummy.data=[];
    this.tmp=""
    for(var i=0; i<this.data.length;i++){
        if(this.Pincode ==this.data[i].Pincode){
          this.dummy.data.push(this.data[i])
        }else if(this.Pincode ==""){
          this.dummy.data.push(this.data[i])
        }
    }
    console.log(this.dummy.data)
  }

  category(event){
      this.cat = event.target.value
      this.Pincode="";
      this.tmp1=1
      this.dummy.data=[];
      for(var i=0; i<this.data.length;i++){
        if(this.data[i].BuyIntrest.search(this.cat)>=0){
          this.dummy.data.push(this.data[i])
        }else if(this.cat==""){
          this.dummy.data.push(this.data[i])
        }
    }
    console.log(this.dummy)
  }

  
  get(){
    this._survey.fetchdata().subscribe((dt:any)=>{
      this.data = dt;
      
    })
  }

  ngOnInit() {
  }

  logout(){
    localStorage.clear();
    this._router.navigateByUrl("login");
  }

  

}
