import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SurveyService } from '../survey.service';

@Component({
  selector: 'app-survey-form',
  templateUrl: './survey-form.component.html',
  styleUrls: ['./survey-form.component.scss']
})
export class SurveyFormComponent implements OnInit {

  title = 'SurveyForm';
  Address;
  FirstName;
  LastName;
  Pincode;
  City;
  Area;
  MonthExpenses;
  Accupation;
  MobileNumber;
  FavoriteStore;
  State;
  RfNumber;
  CustomerShopping;
  Download;
  obj:any=[];

  public data = [];
  public settings = {};
  
  radioChangeHandler(event:any){
    this.CustomerShopping = event.target.value;
  }
  radioChangeHandler1(event:any){
    this.Download = event.target.value;
  }

  ngOnInit() {
    this.data = [
      { item_id: 1, item_text: 'Electronics / Home Appliances' },
      { item_id: 2, item_text: 'Bakery' },
      { item_id: 3, item_text: 'Daily Needs' },
      { item_id: 4, item_text: 'Cloths Shop / Fashion Wears' },
      { item_id: 5, item_text: 'Books' },
      { item_id: 6, item_text: 'Mobile Stores' },
      { item_id: 7, item_text: 'Foot Wears' },
      { item_id: 8, item_text: 'H/W Shops' },
      { item_id: 9, item_text: 'Event Management' },
      { item_id: 10, item_text: 'Hotels & Dhabas / Restaurant' },
      { item_id: 11, item_text: 'Pharmacy' },
      { item_id: 12, item_text: 'Agriculture Fertilizer / Machinery' },
      { item_id: 13, item_text: 'AutoMobile Sale / Car-Bike' },
      { item_id: 14, item_text: 'AutoMobile Services' },
      { item_id: 15, item_text: 'Battery & Inverter' },
      { item_id: 16, item_text: 'Furniture' },
      { item_id: 17, item_text: 'Gold & Silver' },
      { item_id: 18, item_text: 'Surgical Instruments' },
      { item_id: 19, item_text: 'Lights & Jhoomers' },
      { item_id: 20, item_text: 'Hardware' },
      { item_id: 21, item_text: 'Herbal Care Products' },
      { item_id: 22, item_text: 'Home / Building Materials' },
      { item_id: 23, item_text: 'Musical Instruments' },
      { item_id: 24, item_text: 'Glass & Aluminiums' },
      { item_id: 25, item_text: 'Others' }
    ];
    // setting and support i18n
    this.settings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      enableCheckAll: true,
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect all',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 3,
      searchPlaceholderText: 'Search Category',
      noDataAvailablePlaceholderText: 'Không có dữ liệu',
      closeDropDownOnSelection: false,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };
    
  }

  

  myform:FormGroup=new FormGroup({
    FirstName:new FormControl("",[Validators.required,Validators.pattern("[a-z,A-Z]*")]),
    LastName:new FormControl("",[Validators.required,Validators.pattern("[a-z,A-Z]*")]),
    Address:new FormControl("",[Validators.required]),
    City:new FormControl("",[Validators.required,Validators.pattern("[a-z,A-Z]*")]),
    Pincode:new FormControl("",[Validators.required,Validators.pattern("[0-9]*")]),
    Area:new FormControl("",[Validators.required]),
    MonthExpenses:new FormControl("",[Validators.required,Validators.pattern("[0-9]*")]),
    Accupation:new FormControl("",[Validators.required,Validators.pattern("[a-z,A-Z]*")]),
    MobileNumber:new FormControl("",[Validators.required,Validators.pattern("[0-9]*")]),
    FavoriteStore:new FormControl("",[Validators.required,Validators.pattern("[a-z,A-Z]*")]),
    RfNumber:new FormControl("",[Validators.required,Validators.pattern("[0-9]*")]),
    State:new FormControl("",[Validators.required,Validators.pattern("[a-z,A-Z]*")]),
    BuyIntrest:new FormControl("",[Validators.required]),
    CustomerShopping:new FormControl("",[Validators.required]),
    Download:new FormControl("",[Validators.required])    
    
  })

  
  
  
  constructor(private _survey:SurveyService){}
  
  public onFilterChange(item: any) {
    console.log(item);
  }
  public onDropDownClose(item: any) {
    console.log(item);
  }

  public onItemSelect(item: any) {
    console.log(item.item_text);
    this.obj.push(item.item_text);
    
  }
  public onDeSelect(item: any) {
    console.log(item.item_text);
    this.obj.pop(item.item_text);
  }

  public onSelectAll(items: any) {
    
 
    this.obj.push("Electronics / Home Appliances");
    this.obj.push("Bakery");
    this.obj.push("Daily Needs");
    this.obj.push("Cloths Shop / Fashion Wears");
    this.obj.push("Books");
    this.obj.push("Mobile Stores");
    this.obj.push("Foot Wears");
    this.obj.push("H/W Shops"); 
    this.obj.push("Event Management");
    this.obj.push("Hotels & Dhabas / Restaurant"); 
    this.obj.push("Pharmacy"); 
    this.obj.push("Agriculture Fertilizer / Machinery"); 
    this.obj.push("AutoMobile Sale / Car-Bike"); 
    this.obj.push("AutoMobile Services"); 
    this.obj.push("Battery & Inverter"); 
    this.obj.push("Furniture"); 
    this.obj.push("Gold & Silver");
    this.obj.push("Surgical Instruments");
    this.obj.push("Lights & Jhoomers"); 
    this.obj.push("Hardware"); 
    this.obj.push("Herbal Care Products"); 
    this.obj.push("Home / Building Materials");
    this.obj.push("Musical Instruments"); 
    this.obj.push("Glass & Aluminiums"); 
    this.obj.push("Others");
  
  }
  public onDeSelectAll(items: any) {
    this.obj.pop("Electronics / Home Appliances");
    this.obj.pop("Bakery");
    this.obj.pop("Daily Needs");
    this.obj.pop("Cloths Shop / Fashion Wears");
    this.obj.pop("Books");
    this.obj.pop("Mobile Stores");
    this.obj.pop("Foot Wears");
    this.obj.pop("H/W Shops"); 
    this.obj.pop("Event Management");
    this.obj.pop("Hotels & Dhabas / Restaurant"); 
    this.obj.pop("Pharmacy"); 
    this.obj.pop("Agriculture Fertilizer / Machinery"); 
    this.obj.pop("AutoMobile Sale / Car-Bike"); 
    this.obj.pop("AutoMobile Services"); 
    this.obj.pop("Battery & Inverter"); 
    this.obj.pop("Furniture"); 
    this.obj.pop("Gold & Silver");
    this.obj.pop("Surgical Instruments");
    this.obj.pop("Lights & Jhoomers"); 
    this.obj.pop("Hardware"); 
    this.obj.pop("Herbal Care Products"); 
    this.obj.pop("Home / Building Materials");
    this.obj.pop("Musical Instruments"); 
    this.obj.pop("Glass & Aluminiums"); 
    this.obj.pop("Others");
  
  }

  
 
  
  onSubmit(){
    console.log(this.obj)
    this._survey.insertdata({FirstName:this.FirstName, LastName :this.LastName,Address:this.Address, Pincode :this.Pincode,
      City:this.City, Area :this.Area,MonthExpenses:this.MonthExpenses, Accupation :this.Accupation,
      MobileNumber:this.MobileNumber, FavoriteStore :this.FavoriteStore,CustomerShopping:this.CustomerShopping,Download:this.Download,BuyIntrest:this.obj,State:this.State, RfNumber :this.RfNumber}).subscribe((dt:any)=>{
        alert("Your response has been saved"); 
          })
       
     // console.log(this.myform.value) 
      /*this._survey.insertdata(this.myform.value).subscribe((dt)=>{
        console.log("ok");
      })*/
    
      
   }

  

}
